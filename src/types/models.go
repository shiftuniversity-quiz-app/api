package types

type Model struct {
	ID     string `json:"_id" bson:"_id"`
	Title  string `json:"title" bson:"title"`
	A      string `json:"a" bson:"a"`
	B      string `json:"b" bson:"b"`
	C      string `json:"c" bson:"c"`
	D      string `json:"d" bson:"d"`
	Answer string `json:"answer" bson:"answer"`
	V      int    `json:"_v" bson:"_v"`
}

type List struct {
	Questions []Model `json:"questions"`
}

type ENV struct {
	ENV string
}
