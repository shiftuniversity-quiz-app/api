package handlers

import (
	. "api/src/types"
	"os"

	"github.com/gofiber/fiber/v2"
)

func GetHandler(c *fiber.Ctx) error {
	mode := os.Getenv("MODE")

	var questions *List
	var err error

	if mode == "TEST" {
		questions, err = MockGetQuestions()
	} else {
		questions, err = GetQuestions()
	}

	c.JSON(questions)
	c.Status(fiber.StatusOK)
	return err
}

func PostHandler(c *fiber.Ctx) error {
	mode := os.Getenv("MODE")

	model := Model{}

	err := c.BodyParser(&model)

	if err != nil {
		return err
	}

	var data *Model
	var err2 error

	if mode == "TEST" {
		c.Status(fiber.StatusCreated)
	} else {
		err2 = AddQuestion(&model)
	}

	c.JSON(data)
	c.Status(fiber.StatusCreated)
	return err2
}
