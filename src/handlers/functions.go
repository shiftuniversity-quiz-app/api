package handlers

import (
	. "api/src/connections"
	. "api/src/types"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
)

func Endpoints(app *fiber.App) *fiber.App {

	app.Get("/questions", GetHandler)
	app.Post("/questions", PostHandler)

	return app
}

func MockGetQuestions() (*List, error) {
	m1 := Model{ID: "1", Title: "Bla", A: "kayseri", B: "konya", C: "adana", D: "tekirdag", Answer: "a", V: 0}
	m2 := Model{ID: "2", Title: "Bla bla", A: "kayseri", B: "konya", C: "adana", D: "tekirdag", Answer: "a", V: 0}
	m3 := Model{ID: "3", Title: "Bla bla bla", A: "kayseri", B: "konya", C: "adana", D: "tekirdag", Answer: "a", V: 0}

	questionList := List{
		Questions: []Model{m1, m2, m3},
	}

	return &questionList, nil
}

func GetQuestions() (*List, error) {
	client, ctx := MongoClient()
	db := client.Database("shiftQuiz").Collection("questions")

	questionList := List{}

	cursor, err := db.Find(ctx, bson.M{})

	if err != nil {
		return nil, err
	}

	defer cursor.Close(ctx)

	for cursor.Next(ctx) {
		model := Model{}

		cursor.Decode(&model)
		questionList.Questions = append(questionList.Questions, model)
	}

	return &questionList, nil

}

func AddQuestion(m *Model) error {
	if m.Title == "" {
		return fiber.NewError(404, "question should not be empty")
	}

	client, ctx := MongoClient()
	db := client.Database("shiftQuiz").Collection("questions")
	if m.ID == "" {
		m.ID = uuid.New().String()
	}

	db.InsertOne(ctx, m)

	return nil

}
