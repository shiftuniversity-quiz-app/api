package connections

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func MongoClient() (*mongo.Client, context.Context) {
	dbURI := "mongodb+srv://sudanmerinosu:abbas123@test.ge86u.mongodb.net/shiftUniQuiz?retryWrites=true&w=majority"

	ctx, _ := context.WithTimeout(context.Background(), 15*time.Second)
	clientOptions := options.Client().ApplyURI(dbURI)

	client, _ := mongo.Connect(ctx, clientOptions)

	return client, ctx
}
