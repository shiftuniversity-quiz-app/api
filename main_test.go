package main

import (
	. "api/src/handlers"
	. "api/src/types"
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
	"testing"

	"github.com/gofiber/fiber/v2"
	. "github.com/smartystreets/goconvey/convey"
)

func TestAcceptance_QuestionsGet(t *testing.T) {
	Convey("Given question model", t, func() {
		m1 := Model{ID: "1", Title: "question1", A: "answer1", B: "answer2", C: "answer3", D: "answer4", Answer: "a"}
		m2 := Model{ID: "2", Title: "question2", A: "answer1", B: "answer2", C: "answer3", D: "answer4", Answer: "a"}
		m3 := Model{ID: "3", Title: "question3", A: "answer1", B: "answer2", C: "answer3", D: "answer4", Answer: "a"}

		AddQuestion(&m1)
		AddQuestion(&m2)
		AddQuestion(&m3)

		Convey("When I GET request to /questions", func() {
			req, _ := http.NewRequest(http.MethodGet, "/questions", nil)

			req.Header.Add("Content-Type", "application/json")

			app := fiber.New()

			app = Endpoints(app)

			res, err := app.Test(req, 30000)

			So(err, ShouldBeNil)

			Convey("Then should return status code 200", func() {
				So(res.StatusCode, ShouldEqual, fiber.StatusOK)

				Convey("Then should return quiz list", func() {
					resBody, err := ioutil.ReadAll(res.Body)

					So(err, ShouldBeNil)

					returnedData := List{}

					err = json.Unmarshal(resBody, &returnedData)
					So(err, ShouldBeNil)
					So(returnedData.Questions[0].ID, ShouldEqual, m1.ID)
					So(returnedData.Questions[1].ID, ShouldEqual, m2.ID)
					So(returnedData.Questions[2].ID, ShouldEqual, m3.ID)
					So(returnedData.Questions[0].Title, ShouldEqual, m1.Title)
					So(returnedData.Questions[1].Title, ShouldEqual, m2.Title)
					So(returnedData.Questions[2].Title, ShouldEqual, m3.Title)
				})

			})
		})
	})
}

func TestAcceptance_QuestionsPost(t *testing.T) {
	Convey("Given add question body", t, func() {
		m := Model{ID: "1", Title: "question1", A: "answer1", B: "answer2", C: "answer3", D: "answer4", Answer: "a"}

		Convey("When I send a POST req to /questions err should be nil", func() {
			questionByte, err := json.Marshal(m)
			So(err, ShouldBeNil)

			questionReader := bytes.NewReader(questionByte)
			req, err := http.NewRequest(http.MethodPost, "/questions", questionReader)
			So(err, ShouldBeNil)

			req.Header.Add("Content-Type", "application/json")
			req.Header.Set("Content-Length", strconv.Itoa(len(questionByte)))

			app := fiber.New()
			app = Endpoints(app)

			res, err := app.Test(req, 20000)
			So(err, ShouldBeNil)

			Convey("Then status code should be 201", func() {
				So(res.StatusCode, ShouldEqual, fiber.StatusCreated)
			})
		})
	})
}
