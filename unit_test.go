package main

import (
	"testing"

	. "api/src/connections"
	. "api/src/handlers"
	. "api/src/types"

	. "github.com/smartystreets/goconvey/convey"
)

func TestUnit_DBConnection(t *testing.T) {
	Convey("MongoDB connect test", t, func() {
		client, ctx := MongoClient()

		So(ctx, ShouldNotBeNil)
		So(client, ShouldNotBeNil)
	})
}

func TestUnit_EmptyError(t *testing.T) {
	Convey("Empty model", t, func() {
		m := Model{}

		err := AddQuestion(&m)

		So(err, ShouldNotBeNil)

	})
}
